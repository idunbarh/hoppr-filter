# Hoppr Starter Plugin

A basic Hoppr plugin, used in the [https://hoppr.dev Plugin Development](https://hoppr.dev/plugin_development) tutorial.  To run this plugin, first ensure you have Hoppr installed, then do `hopctl bundle starter_files/manifest.yml -t starter_files/transfer.yml -c starter_files/credentials.yml`
