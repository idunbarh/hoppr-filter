from starter_plugin import __version__
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process, hoppr_rerunner
from hoppr.result import Result
from hoppr.context import Context
from typing import Any, Dict, List, Optional, Tuple

class StarterPlugin(HopprPlugin):

    # NOTE: Specify supported_purl_types so your plugin will ignore unsupported purls
    # To support ALL purls, override `supports_purl_type` to always return True
    supported_purl_types = ["binary", "generic", "raw"]

    # NOTE: Specify products to allow In-Toto support in hoppr to find files that were
    # created by this plugin. If the plugin creates files that are not noted here,
    # In-Toto verification will fail. 
    # sample products = ["binary/*", "generic/*", "raw/*"]

    # This sample plugin doesn't modify anything in `collect_root_dir`, so it should
    # have an empty list for products.
    products: List[str] = []

    def get_version(self) -> str:
        """
        __version__ required for all HopprPlugin implementations
        """
        return __version__

    def __init__(self, context: Context, config: Any = None) -> None:
        """
        Constructor with Hoppr framework arguments (context and config)
        """
        super().__init__(context, config)

    # NOTE: The hoppr_process decorator enables the logger and some helpful plugin bookkeeping
    @hoppr_process
    def pre_stage_process(self) -> Result:
        """
        Step 1. Pre-Stage Process (executes once)
        """
        self.get_logger().info('[ PRE Stage Processing here ]')

        # NOTE: Each plugin has a `self.config`, which receives values via the `transfer.yml` file
        config_value = self.config.get('optional_config')
        self.get_logger().info(f'[ Using optional_config={config_value} from starter_files/transfer.yml ]')
        self.get_logger().flush()

        return Result.success()

    @hoppr_process
    # NOTE: The hoppr_rerunner decorator enables re-executing a method if you return Result.retry
    @hoppr_rerunner
    def process_component(self, comp: Any) -> Result:
        """
        Step 2->?? (executes ?? times, where ?? is based on the number of SBOM components to process)
        """
        # NOTE: The "comp" argument is a Pydantic object that represents a CycloneDX Component (see starter_files/bom.json)
        self.get_logger().info(f'[ Processing for Component: {comp.purl} ]')
        self.get_logger().flush()

        return Result.success() # NOTE: Utilize the Result object with success/retry/fail/skip to inform Hoppr how to proceed

    @hoppr_process
    def post_stage_process(self):
        """
        Final Step. Post-Stage Process (executes once - cleanup, finalize, etc)
        """
        self.get_logger().info('[ POST Stage Processing here ]')
        self.get_logger().info('[ Hoppr uses multiprocessing, you may see `process_component` execute in different orders ]')
        self.get_logger().info('[ For pre- and post- stage processing, however, order is always preserved ]')

        return Result.success()